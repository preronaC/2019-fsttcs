\documentclass[dvipsnames]{beamer}
\usetheme{focus}

\usepackage[english] {babel}
\input{macros}
\usepackage{amsmath,amssymb,amsfonts,graphicx}
\usepackage{commons}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}
\usetikzlibrary{matrix}
\usetikzlibrary{calc}

\newcommand{\tmark}[1]{\tikz[remember picture, overlay] \node(#1){};}

\newcommand{\zero}{\mathrm{0}}
\renewcommand{\phi}{\varphi}

\title{Constructing \\Faithful Homomorphisms over\\ Fields of Finite Characteristic}
\author
  {Prerona Chatterjee\\ \vspace{.5em} \small{Joint work with \emph{Ramprasad Saptharishi}}\\ \vspace{1em} Tata Institute of Fundamental Research, Mumbai}

\date{December 11, 2019}
\institute{FSTTCS, IIT Bombay}

\begin{document}
\tikzset{%
	block/.style    = {draw, thick, rectangle, minimum height = 3em,
		minimum width = 3em},
	sum/.style      = {draw, circle, node distance = 2cm}, % Adder
	input/.style    = {coordinate}, % Input
	output/.style   = {coordinate} % Output
}
\newcommand{\suma}{\Large$+$}
\newcommand{\proda}{\Large$\times$}

\maketitle

\section{Faithful Maps}
\begin{frame}{Algebraic Independence}
	\pause
	In the vector space $\R^3$ over $\R$,
	\[
		\uncover<3>{\text{\textcolor{BrickRed}{$1$}} \times}(1,0,1) \uncover<3>{+ \text{\textcolor{BrickRed}{$2$}} \times} (0,1,0) \uncover<3>{- \text{\textcolor{BrickRed}{$1$}} \times} (1,2,1) \uncover<3>{= \text{\textcolor{BrickRed}{$0$}}}
	\]
	\uncover<4->{are \textcolor{ForestGreen}{linearly dependent.}}\\
	
	\vskip 2em
		
	\uncover<5->{In the space of bi-variate polynomials over $\C$,
	\[
	x^2 \uncover<6>{\quad \times \quad} y^2 \uncover<6>{\quad - \quad(} xy \uncover<6>{)\text{\textcolor{BrickRed}{$^2$}}} \quad \uncover<6>{=} \quad \uncover<6>{\text{\textcolor{BrickRed}{$0$}}}
	\]}
	\uncover<7->{are \textcolor{ForestGreen}{algebraically dependent.}}
\end{frame}

\begin{frame}{Algebraic Independence}
	\textbf{Definition}: Suppose $\set{f_1, \ldots, f_k} \subseteq \F[\x]$. \pause They are said to be \textcolor{ForestGreen}{algebraically dependent} if there exists $\text{\textcolor{BrickRed}{$A$}} \in \F[y_1, \ldots, y_k]$ \pause such that 
	\[\text{\textcolor{BrickRed}{$A$}}(y_1, \ldots, y_k) \neq \zero; \qquad \text{\textcolor{BrickRed}{$A$}}(f_1, \ldots, f_k) = \zero.\]\pause
	Otherwise, they are said to be \textcolor{Cyan}{algebraically independent}.\\ \pause
	
	\vskip 2em
	
	\textbf{Note}: The underlying field is very important. \pause For any prime $p$,\\	
	\[x^p + y^p \qquad x+y\]\pause
	\begin{itemize}
		\item are \textcolor{Cyan}{algebraically independent} over $\C$. \pause
		\item are \textcolor{ForestGreen}{algebraically dependent} over $\F_p$.\pause \qquad $[x^p + y^p = (x+y)^p]$
	\end{itemize}
\end{frame}

\begin{frame}{Algebraic Rank}
	\begin{columns}
		\uncover<2->{\begin{column}{.2\textwidth}
			\begin{tikzpicture}[thick]
			\draw (0,0) ellipse (1 and 1.5);
			\node at (0,0) {$\R^3$};
			\end{tikzpicture}
		\end{column}}
		\begin{column}{.7\textwidth}
			\begin{itemize}
				\item \textcolor{BrickRed}{Linear rank} of $S = \set{v_1, \ldots, v_m} \subseteq \mathbb{V}$ is the size of the largest \textcolor{cyan}{linearly independent} subset of $S$. \pause
				\item \textcolor{ForestGreen}{Linear rank} of $\set{(1,0,1), (0,1,0), (1,2,1)}$ is $2$. \pause
			\end{itemize}
		\end{column}
	\end{columns}

	\uncover<3->{\begin{columns}
		\begin{column}{.7\textwidth}
			\begin{itemize}
				\item \textcolor{BrickRed}{Algebraic rank} of $S = \set{f_1, \ldots, f_m} \subseteq \mathbb{\F[\vecx]}$ is the size of the largest \textcolor{cyan}{algebraically independent} subset of $S$.
				\only<3-4>{\uncover<4>{\item \textcolor{ForestGreen}{Algebraic rank} of $\set{x^p + y^p, x+y}$ is \textcolor{BrickRed}{$1$}}}
				\only<5>{\item \textcolor{ForestGreen}{Algebraic rank} of $\set{x^p + y^p, x+y}$ is \textcolor{BrickRed}{$2$}}
			\end{itemize}
		\end{column}
		\begin{column}{.2\textwidth}
			\begin{tikzpicture}[thick]
			\uncover<4-5>{\draw (0,0) ellipse (1 and 1.5);}
			\uncover<4>{\node at (0,0) {$\F_p[x,y]$};}
			\only<5>{\node at (0,0) {$\C[x,y]$};}
			\end{tikzpicture}
		\end{column}
	\end{columns}}
\end{frame}
\begin{frame}{Rank Preserving Maps}
	\textbf{Basis in Linear Algebra}: Given a set of vectors $\set{v_1, v_2, \ldots, v_m}$\\
	\qquad \qquad \qquad with \textcolor{ForestGreen}{linear rank $k$}, there is a basis of \textcolor{cyan}{size $k$}.\\ \pause
	\vspace{1em}
	\begin{block}{Definition: Faithful Maps}
		Given a set of polynomials $\set{\fm}$ with algebraic rank $k$, a map 
		\vspace{-1em}
		\[\phi: \set{\x} \to \F[\yk]\] is said to be a \textcolor{BrickRed}{faithful} map if the \textcolor{ForestGreen}{algebraic rank} \\ \qquad \qquad \qquad \qquad \qquad of $\set{f_1 \circ \phi, f_2 \circ \phi, \ldots, f_m \circ \phi}$ is also $k$.
	\end{block}\pause

	\vspace{.5em}
	\begin{center}
		\textbf{Question}: Can we construct faithful maps efficiently?\\\pause
		\vspace{.5em}
		\textbf{Bonus}: Helps in polynomial identity testing.
	\end{center}
\end{frame}

\begin{frame}{Polynomial Identity Testing}
	\textbf{Given}: Circuit $\ckt$ that computes an $n$-variate, degree $d$ polynomial\\
	\textbf{Goal}: Check whether $\ckt \cong$ Zero Polynomial.\\ \vspace{1em}
	\begin{columns}
		\begin{column}{.3\textwidth}
			\begin{tikzpicture}[thick]
			\uncover<1->{\draw (0,0) circle (.3);
				\draw[->] (0,.3) -- (0,1);
				\node at (.5,.8) {$\ckt$};}
			\uncover<1-2>{
				\draw (-.2,-.25) -- (-1.6,-3.2) -- (1.6,-3.2) -- (.2,-.25);
				\draw (-.3,-1.5) circle (.3);
				\node at (-.3,-1.5) {$\times$};
				\draw (.5,-2.2) circle (.3);
				\node at (.5,-2.2) {$+$};
				\node at (-1.3,-3.7) {$x_1$};
				\node at (-.8,-3.7) {$x_2$};
				\node at (-.1,-3.6) {$\cdots$};
				\node at (.5,-3.6) {$\cdots$};
				\node at (1.3,-3.7) {$x_n$};
				\draw (-1.3,-3.4) -- (-1.3,-3);
				\draw (-.8,-3.4) -- (-.8, -3);
				\draw (1.3,-3.4) -- (1.3,-3);}
			\uncover<3->{
				\node at (0,-1.2) {$\ckt'$};
				\draw (-.2,-.25) -- (-1.5,-2) -- (1.5,-2) -- (.2,-.25);
				\draw (-1.1,-1.9) -- (-1.1,-2.2) -- (-1.6,-2.8) -- (-.6,-2.8) -- (-1.1,-2.2);
				\node at (-1.1,-2.55) {\small{$f_1$}};
				\node at (-.2,-2.55) {$\cdots$};
				\node at (.2,-2.55) {$\cdots$};
				\node at (1.1,-2.55) {\small{$f_m$}};
				\draw (1.1,-1.9) -- (1.1,-2.2) -- (1.6,-2.8) -- (.6,-2.8) -- (1.1,-2.2);
				\draw (-1.4,-2.7) -- (-1.4,-3);
				\draw (-1.3,-2.7) -- (-1.3,-3);
				\node at (-1.05,-3) {$\cdots$};
				\draw (-.8,-2.7) -- (-.8,-3);
				\draw (1.4,-2.7) -- (1.4,-3);
				\draw (1.3,-2.7) -- (1.3,-3);
				\node at (1.05,-3) {$\cdots$};
				\draw (.8,-2.7) -- (.8,-3);
				\draw (-1.3,-3.6) circle (.2);
				\draw (-.5,-3.6) circle (.2);
				\node at (.1,-3.6) {$\cdots$};
				\node at (.6,-3.6) {$\cdots$};
				\draw (1.3,-3.6) circle (.2);
				\draw (-1.3,-3.4) -- (-1.3,-3.2);
				\draw (-.5,-3.4) -- (-.5, -3.2);
				\draw (1.3,-3.4) -- (1.3,-3.2);
				\node at (-1.3,-3.6) {\tiny{$x_1$}};
				\node at (-.5,-3.6) {\tiny{$x_2$}};
				\node at (1.3,-3.6) {\tiny{$x_n$}};}
			\end{tikzpicture}
		\end{column}
		\begin{column}{.7\textwidth}
			\uncover<2->{\textbf{Trivial Upperbound}: \textcolor{BrickRed}{$(d+1)^n$}
			\begin{tabbing}
				\textbf{Approach}: \= Reduce no. of variables\\
				\> Keep degree under control\\
				\> Preserve non-zeroness
			\end{tabbing}}
			\uncover<3->{\textbf{Special Case}: $\ckt = \ckt'(\fm)$ where \\ \vspace{1em}
			\textcolor{ForestGreen}{algebraic rank} of $\set{f_1, \ldots, f_m} = k$, and 
				\[k \ll n\]\\}
			\uncover<4->{
				\begin{center}
					\textbf{Q}: Can the upperbound be made $\approx (d+1)^k$?
			\end{center}}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Faithful Maps and PIT [BMS13, ASSS16]}
	\begin{columns}
		\begin{column}{.3\textwidth}			
			\begin{tikzpicture}[thick]
			\draw (0,0) circle (.3);
			\draw[->] (0,.3) -- (0,1);
			\node at (.75,.8) {$\ckt \circ \phi$};
			\node at (0,-1.2) {$\ckt'$};
			\draw (1.3,-3.6) circle (.2);
			\draw (-.2,-.25) -- (-1.5,-2) -- (1.5,-2) -- (.2,-.25);
			\draw (-1.1,-1.9) -- (-1.1,-2.2) -- (-1.6,-2.8) -- (-.6,-2.8) -- (-1.1,-2.2);
			\node at (-1.1,-2.55) {\small{$f_1$}};
			\node at (-.2,-2.55) {$\cdots$};
			\node at (.2,-2.55) {$\cdots$};
			\node at (1.1,-2.55) {\small{$f_m$}};
			\draw (1.1,-1.9) -- (1.1,-2.2) -- (1.6,-2.8) -- (.6,-2.8) -- (1.1,-2.2);
			\draw (-1.4,-2.7) -- (-1.4,-3);
			\draw (-1.3,-2.7) -- (-1.3,-3);
			\node at (-1.05,-3) {$\cdots$};
			\draw (-.8,-2.7) -- (-.8,-3);
			\draw (1.4,-2.7) -- (1.4,-3);
			\draw (1.3,-2.7) -- (1.3,-3);
			\node at (1.05,-3) {$\cdots$};
			\draw (.8,-2.7) -- (.8,-3);
			\draw (-1.3,-3.6) circle (.2);
			\draw (-.5,-3.6) circle (.2);
			\node at (.1,-3.6) {$\cdots$};
			\node at (.6,-3.6) {$\cdots$};
			\draw (1.3,-3.6) circle (.2);
			\draw (-1.3,-3.4) -- (-1.3,-3.2);
			\draw (-.5,-3.4) -- (-.5, -3.2);
			\draw (1.3,-3.4) -- (1.3,-3.2);
			\node at (-1.3,-3.6) {\tiny{$\phi_1$}};
			\node at (-.5,-3.6) {\tiny{$\phi_2$}};
			\node at (1.3,-3.6) {\tiny{$\phi_n$}};
			\draw (-1.4,-3.75) -- (-1.6,-4.1) -- (-1,-4.1) -- (-1.2,-3.75);
			\draw (-.6,-3.75) -- (-.8,-4.1) -- (-.2,-4.1) -- (-.4,-3.75);
			\draw (1.2,-3.75) -- (1,-4.1) -- (1.6,-4.1) -- (1.4,-3.75);
			\draw (-1.5,-4.05) -- (-1.5,-4.2);
			\draw (-1.4,-4.05) -- (-1.4,-4.2);
			\draw (-1.1,-4.05) -- (-1.1,-4.2);
			\draw (-.7,-4.05) -- (-.7,-4.2);
			\draw (-.6,-4.05) -- (-.6,-4.2);
			\draw (-.3,-4.05) -- (-.3,-4.2);
			\draw (1.1,-4.05) -- (1.1,-4.2);
			\draw (1.2,-4.05) -- (1.2,-4.2);
			\draw (1.5,-4.05) -- (1.5,-4.2);
			\node at (-1.3,-5) {$y_1$};
			\node at (-.7,-5) {$y_2$};
			\node at (-.05,-5) {$\cdots$};
			\node at (.5,-5) {$\cdots$};
			\node at (1.3,-5) {$y_k$};
			\draw (-1.3,-4.4) -- (-1.3,-4.7);
			\draw (-.7,-4.4) -- (-.7, -4.7);
			\draw (1.3,-4.4) -- (1.3,-4.7);
			\end{tikzpicture}
		\end{column}
		\begin{column}{.65\textwidth}
			\textbf{Property required}: $\ckt \neq \zero \implies \ckt \circ \phi \neq \zero$ \pause
			
			\vspace{1em}
			If $k = m$ and $\ckt' \neq \zero$,
			
			\vspace{-.5em}
			\[
			\ckt'(f_1, \ldots, f_k) \neq \zero \pause
			\]
			
			\vspace{-1em}
			Since $\phi$ is faithful, 
			
			\vspace{-.5em}
			\[
				\ckt \circ \phi = \ckt' (f_1 \circ \phi, \ldots, f_k \circ \phi) \neq \zero \pause
			\]
			
			\vspace{-1em}
			Thus,
			\[\ckt \neq \zero \implies \ckt \circ \phi \neq \zero\]\pause
			
			\textbf{Fact}: Even when $k < m$, if $\phi$ is faithful,
			
			\vspace{-.5em}
			\[
				\ckt \neq \zero \implies \ckt \circ \phi \neq \zero
			\]
		\end{column}
	\end{columns}
\end{frame}

\section{Constructing Faithful Maps}

\begin{frame}{The Question}
	Given a set of polynomials $\set{\fm} \subseteq \F[x_1, \ldots, x_n]$, we want to construct a map
	 
	\vspace{-.75em}
	\[\phi: \set{\x} \to \F[\yk]\] 
	such that 
	
	\vspace{-.75em}
	\[
		\text{\textcolor{BrickRed}{$\algrank$}}(f_1(\phi), f_2(\phi), \ldots, f_m(\phi)) = \text{\textcolor{BrickRed}{$\algrank$}}(f_1, f_2, \ldots, f_m)
	\]\pause
	
	\textbf{Fact}: A random affine transformation is a faithful map
	
	\vspace{-.75em}
	\[\phi : x_i = \sum_{j=1}^{k}s_{ij}y_j + a_i\] \pause
	
	\vspace{.5em}
	\textbf{Question}: Can we construct faithful maps \textcolor{ForestGreen}{deterministically}?
	
\end{frame}

\begin{frame}{Characteristic Zero Fields [BMS13, ASSS16]}
	\pause
	\textbf{Step 1}: Capture algebraic rank via linear rank \uncover<5->{of the \textcolor{ForestGreen}{Jacobian}}\\ \pause

	\vspace{1em}
	For $\set{\fm} \subseteq \F[\x]$ and $\vecf = (\fm)$, 
	\[\J_\vecx(\vecf) = 
	\begin{bmatrix} %change order
		\partial_{x_1}(f_1) & \partial_{x_2}(f_1) & \ldots & \partial_{x_n}(f_1)\\
		\partial_{x_1}(f_2) & \partial_{x_2}(f_2) & \ldots & \partial_{x_n}(f_2)\\
		\vdots & \vdots & \ddots & \vdots\\
		\partial_{x_1}(f_m) & \partial_{x_2}(f_m) & \ldots & \partial_{x_n}(f_m)\\
		\end{bmatrix}\] \pause
	\begin{block}{The Jacobian Criterion [Jac41]}
		If $\F$ has characteristic zero, the algebraic rank of $\set{\fm}$ is equal to the linear rank of its Jacobian matrix.
	\end{block}
\end{frame}

\begin{frame}{Characteristic Zero Fields [BMS13, ASSS16]}
	\textbf{Step 2}: Start with a generic linear transformation
	\vspace{-.75em} 
	\only<1-7>{\[\phi : x_i = \sum_{j=1}^{k}s_{ij}y_j + a_i\]}
	\only<8->{\textcolor{BrickRed}{\[\phi : x_i = \sum_{j=1}^{k}s^{ij}y_j + a_i\]}}
	
	\vspace{-3em}
	\pause\[\begin{bmatrix}
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \J_\vecy(\vecf(\phi)) & \text{ }\\ 
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }\\
		\end{bmatrix} \pause
		=
		\begin{bmatrix}
		& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\ 
		& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \phi(\J_\vecx(\vecf)) & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &
		\end{bmatrix}
		\times 
		\begin{bmatrix}
		& \text{ } & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } &\\
		& \text{ } & M_\phi & \text{ } &\\ 
		& \text{ } & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } &\\
		\end{bmatrix}\]\pause
		
	\vspace{-1em}
	\textbf{What we need:} $\phi$ such that
	\begin{itemize}
		\item $\rank(\J_\vecx(\vecf)) = \rank(\phi(\J_\vecx(\vecf)))$ \pause : Can be done if $f_i$s are structured \pause
		\item $M_\phi$ preserves rank \pause : True if $\set{M_\phi[i,j] = s^{ij}}$ $\ldots \ldots \ldots$ [GR05]
	\end{itemize}
\end{frame}

\begin{frame}{What happens over Finite Characteristic Fields?}
	\pause
	The Jacobian Criterion is \textcolor{BrickRed}{\textbf{false}} over finite characteristic fields.\\ \pause
	
	\vspace{1em}
	\textbf{Taylor Expansion}
	
	\vspace{.5em}
	For any $f \in \F[\x]$ and $\vecz \in \F^n$,
	\[f(\vecx + \vecz) - f(\vecz) = \underbrace{x_1 \cdot \partial_{x_1}f + \cdots + x_n \cdot \partial_{x_n}f}_{\text{Jacobian}} + \text{ higher order terms}\]\pause
	
	\vspace{-.75em}
	[PSS18]: Look up till the \textcolor{ForestGreen}{inseparable degree} in the expansion. \pause
	
	\vspace{.25em}
	\begin{columns}
		\begin{column}{.5\textwidth}
			\begin{block}{Definition: A new Operator}
				For any $f \in \F[\x]$,
				\[\Ech_t(f) = \deg^{\leq t} \inparen{f(\vecx + \vecz) - f(\vecz)}\]
			\end{block}
		\end{column}\pause
		\begin{column}{.5\textwidth}
			\[\hat{\Ech}(\vecf) = \begin{bmatrix}
			& \ldots & \Ech_t(f_1) & \ldots & \\
			& \ldots & \Ech_t(f_2) & \ldots & \\
			& & \vdots\\
			& \ldots & \Ech_t(f_k) & \ldots &
			\end{bmatrix}\]
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Alternate Criterion for the General Case [PSS18]}
	$\fk \in \F[\vecx]$ are algebraically independent if and only if for every $\inparen{v_1, v_2, \ldots, v_k}$ with $v_i \text{s in } \I_t$, \pause
	\[\Ech(\vecf, \vecv) = \begin{bmatrix}
	& \ldots & \Ech_t(f_1) + v_1 & \ldots & \\
	& \ldots & \Ech_t(f_2) + v_2 & \ldots & \\
	& & \vdots\\
	& \ldots & \Ech_t(f_k) + v_k & \ldots &
	\end{bmatrix} \pause \text{ has full rank over } \F(\vecz)\]	\pause
	where $t$ is the \textcolor{ForestGreen}{inseparable degree} of $\set{\fk}$ and 
	\[
	\I_t = \genset{\Ech_t(f_1), \Ech_t(f_2), \ldots, \Ech_t(f_k)}^{\geq 2}_{\F(\vecz)} \bmod{\genset{\vecx}^{t+1}} \subseteq \F(\vecz)[\vecx].
	\]
\end{frame}

\begin{frame}{Our Result}
	\begin{tabbing}
		Suppose \quad\= $\circ$ $f_1,\ldots, f_m \in \F[x_1,\ldots, x_n]$\\
		\> $\circ$ algebraic rank of $\set{f_1,\ldots, f_m} = k$\\
		\> $\circ$ \textcolor{ForestGreen}{inseparable degree of $\set{f_1,\ldots, f_m} = t$}
	\end{tabbing}

	\pause Then, we can construct 
	\[
		\Phi:\F[\vecx] \rightarrow \F(s)[y_\zero, y_1,\ldots, y_k]
	\]\pause
	such that
	\[
		\algrank_\F(f_1 \circ \Phi, \ldots, f_m \circ \Phi) = k
	\]\pause
	whenever
	\begin{itemize}
		\item each of the $f_i$'s are sparse polynomials,
		\item each of the $f_i$'s are products of variable disjoint, multilinear, sparse polynomials.
	\end{itemize}
\end{frame}

\begin{frame}{Proof Overview}
	\textbf{Step 1}: Capture algebraic rank via linear rank of the \textcolor{ForestGreen}{PSS-Jacobian}\\ \pause
	
	\vspace{1em}
	\textbf{Step 2}: For a \emph{generic linear map} $\Phi: \vecx \rightarrow \F(s)[y_1,\ldots, y_k]$,\\ \qquad write \textbf{PSS} $\J_\vecy(\vecf \circ \Phi)$ in terms of \textbf{PSS} $\J_\vecx(\vecf)$. \pause This can be\\ \qquad described succinctly as
	\vspace{-.5em}
	\[
		\text{\textbf{PSS} }\J_\vecy(f \circ \Phi) = \Phi(\text{\textbf{PSS} }\J_\vecx(\vecf)) \cdot M_\Phi. 
	\] \pause
	
	\vspace{-1em}
	\textbf{What we need:} $\Phi$ such that
	\begin{itemize}
		\item $\rank(\Phi(\text{\textbf{PSS} }\J_\vecx(\vecf))) = \rank(\text{\textbf{PSS} }\J_\vecx(\vecf))$: Can be done if $\vecf$'s are\\ \qquad \qquad some structured polynomials (for example, \textcolor{ForestGreen}{sparse}). \pause
		\item $M_\Phi$ preserves rank. That is,
		
		\vspace{-.5em}
		\[
		\rank(\Phi(\text{\textbf{PSS} }\J_\vecx(\vecf)) \cdot M_\Phi) = \rank(\Phi(\text{\textbf{PSS} }\J_\vecx(\vecf))).
		\]
	\end{itemize}
\end{frame}

\begin{frame}{The Faithful Map}
	\begin{tikzpicture}
		\uncover<1->{\draw[thick] (0cm,0cm) rectangle (4cm,6cm); 
		\node(rows) at (-.5cm, 3cm) {$\vecx^\vece$};
		\draw[->, thick] ($(rows)+(0cm,.5cm)$) -- (-.5cm,6cm);
		\draw[->, thick] ($(rows)+(0cm,-.5cm)$) -- (-.5cm,0cm);
		\node(cols) at (2cm, -.5cm) {$\vecy^\vecd$};
		\draw[->, thick] ($(cols)+(.5cm,0cm)$) -- (4cm,-.5cm);
		\draw[->, thick] ($(cols)+(-.5cm,0cm)$) -- (0cm,-.5cm);
		\draw[thick] (.5cm,5.25cm) rectangle (0cm,6cm);
		\node(one) at (.25cm, 5.625cm) {$1$};
		\draw[thick] (.5cm,5.25cm) rectangle (1.25cm,4.25cm);
		\node(two) at (.825cm, 4.75cm) {$2$};
		\node(ddots1) at (1.5cm, 3.75cm) {$\ddots$};
		\node(ddots2) at (2cm, 3cm) {$\ddots$};
		\draw[thick] (2.5cm,2.5cm) rectangle (4cm,0cm);
		\node(tee) at (3.25cm, 1.25cm) {$t$};	\node(entry) at (7.5cm, 5.5cm) {$M_\Phi(\vecx^\vece, \vecy^\vecd) = \coeff_{\vecy^\vecd}(\Phi(\vecx^\vece))$};}
		\uncover<2->{\node at (7.5cm, 4cm) {\textbf{Taking inspiration from the}};
		\node at (7.5cm, 3.5cm) {\textbf{prev. case}: $M_\Phi(x_i, y_j) = s^{\wt(i)j}$};} 
		\uncover<3->{\node at (7.75cm, 2.5cm) {For the correct definition of};
		\node at (7.75cm, 2cm) {$\wt(i)$, things work out.};}
		\uncover<4->{\node at (7.75cm, 0.5cm) {\textcolor{BrickRed}{$\Phi(x_i) = a_i \cdot y_\zero + \sum_{j \in [k]} s^{\wt(i)j} \cdot y_j$}};}
	\end{tikzpicture}
\end{frame}

\begin{frame}{Open Threads}
	\begin{enumerate}
		\item Construct $\F(s)$-Faithful maps over arbitrary fields.\\ \pause
		\vspace{1em}
		\item Improve the dependence on "inseparable degree".\\ \pause
		\vspace{1em}
		\item $\text{[GSS'18]}$: Different characterisation for Algebraic dependence - not algorithmic but has no dependence on "inseparable degree"\\
		\vspace{.5em}
		Can we get PIT applications out of it?\pause
	\end{enumerate}
	\vspace{3em}
	\begin{center}
		\textbf{\large{Thank you!}}
	\end{center}
\end{frame}
\end{document}

\section{Arbitrary Fields:\\ The PSS Criterion [PSS'16]}

\begin{frame}{What goes wrong over arbitrary fields?}
	Jacobian Matrix has partial derivatives as entries \pause
	- Entries can start becoming zero \pause 
	: \qquad Not the only case.\\ \pause
	\vspace{1em}
	\begin{center}
		$f_1 = xy^{p-1}$, $f_2 = x^{p-1}y$ : Algebraically Independent over $\F_p$.\pause
	\end{center}
	\[\J_{x,y} = \begin{bmatrix}
	y^{p-1} & (p-1)xy^{p-2}\\
	(p-1)x^{p-2}y & x^{p-1}
	\end{bmatrix}\]
	\[\det(\J_{x,y}) = (xy)^{p-1} - (p^2 - 2p + 1) (xy)^{p-1} = \zero \text{ over }\F_p.\] \pause
	
	\textbf{Characteristic Zero}: \qquad $\J$ has full rank $\Leftarrow$ $\J$ has an inverse\\ \pause
	\vspace{1em}
	\textbf{Finite Characteristic}: Entries in "inverse" have denominators that are partial derivatives of some annihilators - Can become zero.
\end{frame}

\begin{frame}{Looking Further in the Taylor Expansion}
	For any $f \in \F[\x]$ and $\vecz \in \F^n$,
	\[f(\vecx + \vecz) - f(\vecz) = \underbrace{x_1 \cdot \partial_{x_1}f + \cdots + x_n \cdot \partial_{x_n}f}_{\text{Jacobian}} + \text{ higher order terms}\]\pause
	
	[PSS'16]: Look at Taylor expansions up to the "inseparable degree". \pause
	
	\vspace{1em}
	\begin{columns}
		\begin{column}{.55\textwidth}
			\begin{block}{A New Operator}
				For any $f \in \F[\x]$,
				\[\Ech_t(f) = \deg^{\leq t} \inparen{f(\vecx + \vecz) - f(\vecz)}\]
			\end{block}
		\end{column}\pause
		\begin{column}{.45\textwidth}
			\[\hat{\Ech}(\vecf) = \begin{bmatrix}
			& \ldots & \Ech_t(f_1) & \ldots & \\
			& \ldots & \Ech_t(f_2) & \ldots & \\
			& & \vdots\\
			& \ldots & \Ech_t(f_k) & \ldots &
			\end{bmatrix}.\]
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{The PSS Criterion}
	A given set of polynomials $\set{\fk} \in \F[\x]$ is algebraically independent if and only if for a random $\vecz \in \F^n$, $\set{\Ech_t(f_1), \Ech_t(f_2), \ldots, \Ech_t(f_k)}$ are linearly independent in  \[\frac{\F(\vecz)[\x]}{\I_t}\] where $t$ is the inseparable degree of $\set{\fk}$ and 
	\[
	\I_t = \genset{\Ech_t(f_1), \Ech_t(f_2), \ldots, \Ech_t(f_k)}^{\geq 2}_{\F(\vecz)} \bmod{\genset{\vecx}^{t+1}} \subseteq \F(\vecz)[\vecx].
	\]
\end{frame}

\begin{frame}{Alternate Statement for the PSS Criterion}
	
	$\set{\fk}$ is algebraically independent if and only if for every $\inparen{v_1, v_2, \ldots, v_k}$ with $v_i \text{s in } \I_t$,
	\[\Ech(\vecf, \vecv) = \begin{bmatrix}
	& \ldots & \Ech_t(f_1) + v_1 & \ldots & \\
	& \ldots & \Ech_t(f_2) + v_2 & \ldots & \\
	& & \vdots\\
	& \ldots & \Ech_t(f_k) + v_k & \ldots &
	\end{bmatrix} \text{ has full rank over } \F(\vecz).\]
\end{frame}

\begin{frame}{The Goal}	
	\textbf{Define}: $\mathcal{F} = \set{\phi : \set{x_1, \ldots, x_n} \to \F[y_1, \ldots y_k]}$ with $\abs{\mathcal{F}} \approx \poly(n)$.
	
	\vspace{.5em}
	\textbf{Required}: \qquad $\ckt = \ckt'(f_1, \ldots, f_m) \neq \zero$, $\algrank(f_1, \ldots, f_m) \leq k$\\
	\qquad \qquad $\implies$ for some $\phi \in \mathcal{F}$, $\ckt_\zero = \ckt'(f_1 \circ \phi, \ldots, f_m \circ \phi) \neq \zero.$ \pause
	
	\vspace{1.5em}	
	\textbf{Sufficient}: \qquad Let $g_i = f_i \circ \phi$ and $\Ech_t(g) = \deg^{\leq t} \inparen{g(\vecy + \vecv) - g(\vecv)}$. Then,
	\[\Ech(\vecf \circ \phi, \vecu) = \begin{bmatrix}
	& \ldots & \Ech_t(f_1 \circ \phi) + u_1 & \ldots & \\
	& \ldots & \Ech_t(f_2 \circ \phi) + u_2 & \ldots & \\
	& & \vdots\\
	& \ldots & \Ech_t(f_m \circ \phi) + u_m & \ldots &
	\end{bmatrix}\] 
	\begin{center}
		has full rank for every $u_1, u_2, \ldots, u_m \in \I_t(\phi)$ where
		\[
		\I_t(\phi) = \genset{\Ech_t(g_1), \ldots, \Ech_t(g_m)}^{\geq 2}_{\F(\vecg(\vecv))} \bmod{\genset{\vecy}^{t+1}} \subseteq \F(\vecv)[\vecy].
		\]
	\end{center}
\end{frame}

\section{Constructing Faithful Homomorphisms over Arbitrary Fields}

\begin{frame}{What to ask for from the map}
	\[\phi : x_i \to \sum_{j=1}^{k}a_{ij}y_j + b_iy_\zero \qquad \text{ and } \qquad \phi_z : z_i \to \sum_{j=1}^{k}a_{ij}w_j + b_iw_\zero\] \pause
	
	\vspace{1em}
	\underline{\textbf{Sufficient Properties}}
	\begin{enumerate}
		\vspace{.5em}
		\item For every $\vecu \in \I_t(\phi)$, there is a $\vecv \in \I_t$ for which $\vecu = \phi_z(\vecv \circ \phi)$\\ \pause
		\vspace{.5em}
		\item $\Ech(\vecf \circ \phi, \vecu) = \phi_z(\Ech(\vecf,\vecv)) \times M_\phi$: Chain Rule \pause
		\vspace{.5em}
		\item $\rank(\Ech(\vecf, \vecv)) = \rank(\phi_z(\Ech(\vecf, \vecv)))$: $b_i$s are responsible for this \pause
		\vspace{.5em}
		\item $M_\phi$ preserves rank
	\end{enumerate}
\end{frame}

\begin{frame}{The Matrix Decomposition}	
	\[\hspace{-1em}
	\begin{bmatrix}
	\text{ } & \text{ } & \text{ } \\
	\text{ } & \Ech(\vecf \circ \phi, \vecu) & \text{ } \\
	\text{ } & \text{ } & \text{ } 
	\end{bmatrix}
	=
	\overbrace{\begin{bmatrix}
		& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\ 
		& \text{ } & \text{ } & \phi_z(\Ech(\vecf,\vecv)) & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &
		\end{bmatrix}}^{\text{ labelled by }\vecx^\vece}
	\times 
	\underbrace{\begin{bmatrix}
		& & \text{ } & \text{ } & \text{ } & &\\
		& & \text{ } & \text{ } & \text{ } & &\\
		& & \text{ } & \text{ } & \text{ } & &\\
		& & \text{ } & M_{\phi} & \text{ } & &\\ 
		& & \text{ } & \text{ } & \text{ } & &\\
		& & \text{ } & \text{ } & \text{ } & &\\
		& & \text{ } & \text{ } & \text{ } &
		\end{bmatrix}}_{\text{labelled by } \vecy^\vecd}
	\]\pause
	where
	\[
	M_\phi(\vecx^\vece, \vecy^\vecd) = \left\{
	\begin{array}{ll}
	\coeff_{\vecy^\vecd}(\phi(\vecx^\vece)) & \mbox{if } \sum e_i = \sum d_i \\
	\zero & \mbox{ otherwise} 
	\end{array}
	\right.
	\]
\end{frame}

\begin{frame}{What makes Vandermonde type matrices work?}
	\only<1-2>{\[\begin{bmatrix}
		& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\ 
		& \text{ } & \text{ } & A & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &
		\end{bmatrix}
		\times 
		\begin{bmatrix}
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }\\
		\text{ } & M & \text{ }\\ 
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }
		\end{bmatrix}
		=
		\begin{bmatrix}
		& \text{ } &\\
		& AM &\\ 
		& \text{ } &
		\end{bmatrix}\]
		
		\vspace{2em}}
	
	\only<2->{ 
		\begin{center}
			\textbf{Cauchy-Binet}: $\det(AM) = \sum_{B \subseteq \set{x_i} \text{, } \abs{B} = k} \det(A_B) \det(M_B).$
	\end{center}}
	
	\only<3>{\[\begin{bmatrix}
		s & s^2 & \ldots & s^k\\
		s^2 & s^4 & \ldots & s^{2k}\\
		\vdots & \vdots & \text{ } & \vdots\\
		\vdots & \vdots & \ddots & \vdots\\
		\vdots & \vdots & \ddots & \vdots\\
		\vdots & \vdots & \text{ } & \vdots\\
		s^n & s^{2n} & \ldots & s^{kn}
		\end{bmatrix}\]}
	\only<4>{\[\begin{bmatrix}
		s & \ldots & s^k\\
		\inparen{s^2}^1& \ldots & \inparen{s^2}^k\\
		\vdots & \text{ } & \vdots\\
		\vdots & \ddots & \vdots\\
		\vdots & \ddots & \vdots\\
		\vdots & \text{ } & \vdots\\
		\inparen{s^n}^1 & \ldots & \inparen{s^n}^k
		\end{bmatrix}\]}
	\only<5->{\[\begin{array}{r}
		x_1\\
		x_2\\
		\vdots\\
		\vdots\\
		\vdots\\
		\vdots\\
		x_n
		\end{array}
		\begin{bmatrix}
		\inparen{s^{\wt(x_1)}}^1 & \ldots & (s^{\wt(x_1)})^k\\
		\inparen{s^{\wt(x_2)}}^1& \ldots & \inparen{s^{\wt(x_2)}}^k\\
		\vdots & \text{ } & \vdots\\
		\vdots & \ddots & \vdots\\
		\vdots & \ddots & \vdots\\
		\vdots & \text{ } & \vdots\\
		\inparen{s^{\wt(x_n)}}^1 & \ldots & \inparen{s^{\wt(x_n)}}^k
		\end{bmatrix} 
		\quad \quad 
		\only<5-8>{\wt(x_i) = i}
		\only<9->{\wt(x_i) \text{ is distinct for each } i}\]}
	
	\only<6->{\begin{itemize}
			\item If $B = \inparen{x_{i_1}, x_{i_2}, \ldots, x_{i_k}}$, then $\wt(B) = \sum_{j=1}^{k} j \wt(x_{i_j})$ \pause
			\uncover<7->{\item $\deg_s(\det(M_B)) = \wt(B)$} \pause
			\uncover<8->{\item Isolate a unique non-zero minor of $A$ with maximum weight}
	\end{itemize}}	
\end{frame}

\begin{frame}{The Current Matrix}
	\begin{tikzpicture}
	\draw[thick] (0cm,0cm) rectangle (4cm,6cm); 
	\node(rows) at (-.5cm, 3cm) {$\vecx^\vece$};
	\draw[->, thick] ($(rows)+(0cm,.5cm)$) -- (-.5cm,6cm);
	\draw[->, thick] ($(rows)+(0cm,-.5cm)$) -- (-.5cm,0cm);
	\node(cols) at (2cm, -.5cm) {$\vecy^\vecd$};
	\draw[->, thick] ($(cols)+(.5cm,0cm)$) -- (4cm,-.5cm);
	\draw[->, thick] ($(cols)+(-.5cm,0cm)$) -- (0cm,-.5cm);
	\draw[thick] (.5cm,5.25cm) rectangle (0cm,6cm);
	\node(one) at (.25cm, 5.625cm) {$1$};
	\draw[thick] (.5cm,5.25cm) rectangle (1.25cm,4.25cm);
	\node(two) at (.825cm, 4.75cm) {$2$};
	\node(ddots1) at (1.5cm, 3.75cm) {$\ddots$};
	\node(ddots2) at (2cm, 3cm) {$\ddots$};
	\draw[thick] (2.5cm,2.5cm) rectangle (4cm,0cm);
	\node(tee) at (3.25cm, 1.25cm) {$t$};	\node(entry) at (7.5cm, 5.5cm) {$M_\phi(\vecx^\vece, \vecy^\vecd) = \coeff_{\vecy^\vecd}(\phi(\vecx^\vece))$};\pause
	\node at (7.5cm, 4cm) {\textbf{Taking inspiration from the}};
	\node at (7.5cm, 3.5cm) {\textbf{prev. case}: $M_\phi(x_i, y_j) = s^{\wt(i)j}$}; \pause
	\node at (7.75cm, 2.75cm) {$\wt(\vecx^\vece) = \sum_{i \in [n]} e_i \wt(i)$};
	\pause
	\node at (7.75cm, 2cm) {$M_\phi(\vecx^\vece, y_j^d) = s^{\wt(\vecx^\vece)j}$};
	\pause
	\node at (7.75cm, .75cm) {If $B = \inparen{\vecx^{\vece_1}, \vecx^{\vece_2}, \ldots, \vecx^{\vece_k}}$,};
	\node at (7.75cm, 0cm) {then $\wt(B) = \sum_{j \in [k]} j \wt(\vecx^{\vece_j})$};
	\end{tikzpicture}
\end{frame}

\begin{frame}{A Rank Preserving Matrix}
	\[\uncover<2->{\begin{array}{r}
		\uparrow\\
		k\\
		\downarrow\\
		\end{array}}
	\begin{bmatrix}
	& \text{ } & \tmark{a}\text{ } & \text{ } & \text{ } & \text{ } &\\ 
	& \text{ } & \text{ } & A & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } & \text{ }\tmark{b} & \text{ } &
	\end{bmatrix}
	\times 
	\only<1-4>{\begin{bmatrix}
		& \text{ } & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } &\\
		& \text{ } & M & \text{ } &\\ 
		& \text{ } & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } &
		\end{bmatrix}
		=
		\begin{bmatrix}
		\text{ } & \text{ } & \text{ }\\
		\text{ } & AM & \text{ }\\ 
		\text{ } & \text{ } & \text{ }
		\end{bmatrix}}
	\only<5->{\begin{bmatrix}
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }\\
		\text{ } & M' & \text{ }\\ 
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }
		\end{bmatrix}
		=
		\begin{bmatrix}
		& \text{ } &\\
		& AM' &\\ 
		& \text{ } &
		\end{bmatrix}}
	\]\pause
	\begin{tikzpicture}[remember picture, overlay]
	\only<2-4>{\node (t) at ($(6.325cm, -.125cm)$) {$>k$};
		\draw[->, thick] ($(5.825cm, -.125cm)$) -- ($(5.125cm, -.125cm)$);
		\draw[->, thick] ($(6.825cm, -.125cm)$) -- ($(7.5cm, -.125cm)$);}
	\only<5->{\node (t) at ($(6.325cm, -.125cm)$) {$k$};
		\draw[->, thick] ($(6.125cm, -.125cm)$) -- ($(5.5cm, -.125cm)$);
		\draw[->, thick] ($(6.5cm, -.125cm)$) -- ($(7.325cm, -.125cm)$);}
	\only<4->{\draw[blue, thick] ($(a)+(.05cm,.1cm)$) rectangle ($(b)-(.05cm,0cm)$);} 
	\end{tikzpicture}\pause
	
	\vspace{2em}
	\textbf{What we want}: $k$ columns of $AM$ that are linearly independent.\\ \pause
	\vspace{1em}
	\textbf{Proof Strategy}: 
	\begin{itemize}
		\item Isolate a unique non-zero minor $A_{B_\zero}$ with maximum weight \pause
		\item $M' \equiv$ $k$ columns of $M$ such that $\deg_s(\det(M'_{B_\zero})) = \wt(B_\zero)$
	\end{itemize}
\end{frame}

\begin{frame}{A few details}
	\textbf{About $\deg_s(\det(M'_{B_\zero}))$ for $B \neq B_\zero$}:\\
	\begin{itemize}
		\item $\deg_s(\det(M'_B)) \leq \wt(B)$ for $B \neq B_\zero$ \pause
	\end{itemize}
	\vspace{1em}
	\textbf{About $\wt$}:\\
	\begin{itemize}
		\item $\wt$ "hashes" the monomials in question\\ $\Rightarrow$ there is a unique $B$ of maximum weight. \pause
	\end{itemize}
	\vspace{1em}
	\textbf{About $M'$}
	\begin{itemize}
		\item $M'$ can always be chosen such that its columns are indexed by "pure" monomials.
	\end{itemize}
\end{frame}

\begin{frame}{A Faithful Map over Arbitrary Fields}
	\vspace{-1.5em}
	\[\phi: x_i \to \sum_{j=1}^{k}s^{\wt(i)j} y_j + a_iy_\zero \quad \text{ and } \quad \phi_z: z_i \to \sum_{j=1}^{k}s^{\wt(i)j} w_j + a_iy_\zero\] where $t$ is the inseparable degree and $\wt(i) = (t+1)^i \bmod p$.\pause \\
	\vspace{1em}
	\underline{\textbf{Properties}}
	\begin{enumerate}
		\vspace{.5em}
		\item For every $\vecu \in \I_t(\phi)$, there is a $\vecv \in \I_t$ for which $\vecu = \phi_z(\vecv \circ \phi)$\\ \pause
		\vspace{.5em}
		\item $\phi_z(\Ech(\vecf,\vecv)) \times M_\phi$ is a sub-matrix of $\Ech(\vecf \circ \phi, \vecu)$\\ \pause
		\vspace{.5em}
		\item $\rank(\phi_z(\Ech(\vecf,\vecv))) = \rank(\phi_z(\Ech(\vecf,\vecv)) \times M_\phi)$\\ \pause
		\vspace{.5em}
		\item $\rank(\Ech(\vecf, \vecv)) = \rank(\phi_z(\Ech(\vecf,\vecv)))$
	\end{enumerate}
\end{frame}

